import { gql } from 'apollo-boost'

export const GET_ITEMLISTS = gql`
  query ItemList($ItemType: String!, $ItemCat: String!) {
    itemlists(ItemType: $ItemType, ItemCat: $ItemCat) {
      id
      ItemCode
      ItemType
      ItemCat
      ItemCatID
      AccountsCode
      ItemDesc
      PortID
      Status
      Ordering
    }
  }
`
