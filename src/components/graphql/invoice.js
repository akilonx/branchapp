import { gql } from 'apollo-boost'

export const GET_INVOICES = gql`
  {
    invoices {
      id
      InvoiceID
      InvoiceNo
      TotalConsignment
      Amount
      InvoiceDate
      InvoiceDate2
    }
  }
`
