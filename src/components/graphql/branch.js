import { gql } from 'apollo-boost'

export const GET_BRANCHES = gql`
  {
    branches {
      BranchName
      BranchCode
    }
  }
`

export const GET_GROUPBRANCHES = gql`
  {
    groupbranches {
      BranchName
      BranchCode
    }
  }
`
