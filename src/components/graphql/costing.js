import { gql } from 'apollo-boost'

export const GET_COSTINGSELECT = gql`
  {
    costingselect {
      id
      ItemCode
      ItemType
      ItemCat
      ItemCatID
      AccountsCode
      ItemDesc
      PortID
      Status
      Ordering
    }
  }
`

export const SEARCH_COSTINGAWB = gql`
  query SearchCostingAwb($AWBNo: String) {
    searchcostingawb(AWBNo: $AWBNo) {
      id
      AWBNo
      AWBID
      MAWBID
      AirlineID
      AgentName
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      TotalWeight
      TotalAmount
      TotalPieces
      TotalConsignment
      ConsignmentList
      CoLoaderName
      MAWBNo
      MConsigneeName
      MJobNo
    }
  }
`

export const GET_BILLINGHEADERBYINVOICETO = gql`
  {
    billingheaderbyinvoiceto {
      id
      AWBNo
      AWBID
      BillDate
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      CNOn
      Pieces
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceNo
      InvoiceDate
      InvoiceToName2
      InvoiceToCode
      Image
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      BillBy
      lastUpdatedBy
      lastUpdated
      MAWBNo
    }
  }
`

export const GET_BILLINGHEADERBYINVOICENO = gql`
  query Billingheaderbyinvoiceno($AWBNo: String) {
    billingheaderbyinvoiceno(AWBNo: $AWBNo) {
      id
      AWBNo
      AWBID
      AirlineID
      AgentName
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      TotalWeight
      TotalAmount
      TotalPieces
      TotalConsignment
      ConsignmentList
      CoLoaderName
      MAWBNo
      MConsigneeName
      MJobNo
    }
  }
`

export const GET_VENDORINVOICES = gql`
  query VendorInvoices($MAWBID: ID, $CustomerID: ID) {
    vendorinvoices(MAWBID: $MAWBID, CustomerID: $CustomerID) {
      id
      CustomerID
      MAWBID
      InvoiceNo
      InvoiceDate
      InvoiceDueDate
      VendorName
      Approved
    }
  }
`

export const GET_COSTING = gql`
  query Costing($AWBID: ID, $VendorCode: String) {
    costings(AWBID: $AWBID, VendorCode: $VendorCode) {
      id
      CostType
      ConsignmentID
      AWBID
      MAWBID
      InvoiceNo
      Amount
      Title
      VendorCode
      InvoiceDate
      CreatedDate
      CreatedBy
      ModifiedDate
      ModifiedBy
      UploadFile
      AirFreightCharges
      FuelCharges
      TerminalScreeningFee
      EHUHandlingCharges
      AWBFee
      OtherLocalChargesAmount
      OtherLocalChargesTitle
      DeliveryCharges
      TerminalCharges
      CustomClearance
      DocumentAirportHandling
      OtherEastMalaysiaAmount
      OtherEastMalaysiaTitle
      EHUHandlingChargesEM
      CostingID
      CostingCode
      VendorName
    }
  }
`

export const GET_UPLOADVENDORINVOICES = gql`
  query UploadVendorInvoices($InvoiceID: ID) {
    uploadvendorinvoices(InvoiceID: $InvoiceID) {
      id
      FileName
      FileType
      FileSize
      CreatedBy
      CreatedOn
    }
  }
`

export const REMOVE_VENDORINVOICE = gql`
  mutation RemoveVendorInvoice($InvoiceID: ID!) {
    removevendorinvoice(InvoiceID: $InvoiceID)
  }
`

export const REMOVE_COSTING = gql`
  mutation RemoveCosting($id: ID!) {
    removecosting(id: $id)
  }
`

export const UPDATE_COSTING = gql`
  mutation UpdateCosting(
    $id: ID
    $CostType: String
    $ConsignmentID: Int
    $AWBID: Int
    $MAWBID: Int
    $InvoiceNo: String
    $Amount: Float
    $Title: String
    $VendorCode: String
    $CostingID: ID
    $CostingCode: String
  ) {
    updatecosting(
      id: $id
      CostType: $CostType
      ConsignmentID: $ConsignmentID
      AWBID: $AWBID
      MAWBID: $MAWBID
      InvoiceNo: $InvoiceNo
      Amount: $Amount
      Title: $Title
      VendorCode: $VendorCode
      CostingID: $CostingID
      CostingCode: $CostingCode
    ) {
      id
      CostType
      ConsignmentID
      AWBID
      MAWBID
      InvoiceNo
      Amount
      Title
      VendorCode
      InvoiceDate
      CreatedDate
      CreatedBy
      ModifiedDate
      ModifiedBy
      UploadFile
      CostingID
      CostingCode
      VendorName
    }
  }
`

export const CREATE_COSTING = gql`
  mutation CreateCosting(
    $CostType: String
    $ConsignmentID: Int
    $AWBID: Int
    $MAWBID: Int
    $InvoiceNo: String
    $Amount: Float
    $Title: String
    $VendorCode: String
    $CostingID: ID
    $CostingCode: String
  ) {
    createcosting(
      CostType: $CostType
      ConsignmentID: $ConsignmentID
      AWBID: $AWBID
      MAWBID: $MAWBID
      InvoiceNo: $InvoiceNo
      Amount: $Amount
      Title: $Title
      VendorCode: $VendorCode
      CostingID: $CostingID
      CostingCode: $CostingCode
    ) {
      id
      CostType
      ConsignmentID
      AWBID
      MAWBID
      InvoiceNo
      Amount
      Title
      VendorCode
      InvoiceDate
      CreatedDate
      CreatedBy
      ModifiedDate
      ModifiedBy
      UploadFile
      AirFreightCharges
      FuelCharges
      TerminalScreeningFee
      EHUHandlingCharges
      AWBFee
      OtherLocalChargesAmount
      OtherLocalChargesTitle
      DeliveryCharges
      TerminalCharges
      CustomClearance
      DocumentAirportHandling
      OtherEastMalaysiaAmount
      OtherEastMalaysiaTitle
      EHUHandlingChargesEM
      CostingID
      CostingCode
      VendorName
    }
  }
`
