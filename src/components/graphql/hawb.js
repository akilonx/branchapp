import { gql } from 'apollo-boost'

export const ADD_HAWB = gql`
  mutation CreateHawb(
    $AWBNo: String!
    $AWBID: ID
    $AgentID: String
    $Remark: String
  ) {
    createHawb(
      AWBNo: $AWBNo
      AWBID: $AWBID
      AgentID: $AgentID
      Remark: $Remark
    ) {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      Express
      CoLoaderName
      AgentName
    }
  }
`

export const UPDATE_HAWB = gql`
  mutation UpdateHawb(
    $id: ID!
    $AWBNo: String!
    $AWBID: ID
    $AgentID: String
    $Remark: String
  ) {
    updateHawb(
      id: $id
      AWBNo: $AWBNo
      AWBID: $AWBID
      AgentID: $AgentID
      Remark: $Remark
    ) {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      Express
      CoLoaderName
      AgentName
    }
  }
`

export const SEARCH_HAWB = gql`
  query SearchHawb($HAWBNo: String!) {
    searchhawb(HAWBNo: $HAWBNo) {
      id
      MAWBNo
      MAWBID
      AWBNo
    }
  }
`
export const GET_AWB_SELECT = gql`
  {
    awbselect {
      id
      AWBNo
    }
  }
`

export const GET_HAWB = gql`
  {
    houseawbs {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      Express
      CoLoaderName
      AgentName
    }
  }
`

export const GET_HAWB_SINGLE = gql`
  query Hawb($id: ID!) {
    hawb(id: $id) {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      AgentName
      ModifiedBy
      LastModified
      InvoiceDate
      InvoiceNo
      BillDate
      BillBy
      Express
      CoLoaderName
      AgentName
      Consignments {
        ConsignmentNo
        Pieces
        Weight
        Destination
      }
    }
  }
`

export const SEARCH_HAWB_SINGLE = gql`
  query SearchHawb($AWBNo: String!) {
    searchhawb(AWBNo: $AWBNo) {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      AgentName
      ModifiedBy
      LastModified
      InvoiceDate
      InvoiceNo
      BillDate
      BillBy
      Express
      CoLoaderName
      AgentName
      Consignments {
        ConsignmentNo
        Pieces
        Weight
        Destination
      }
    }
  }
`

export const DELETE_HAWB = gql`
  mutation DeleteHawb($id: ID!) {
    deleteHawb(id: $id)
  }
`

export const GET_HAWB_BY_MAWBID = gql`
  query HawbByMawbid($id: ID!) {
    hawbbymawbid(id: $id) {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      State
      Express
      CoLoaderName
      AgentName
    }
  }
`

export const GET_HAWB_NOTLOCKED = gql`
  {
    hawbsnotlocked {
      id
      MAWBNo
      MAWBID
      AWBNo
      CoLoaderCode
      ETD
      ETA
      POL
      POD
      AirlineID
      FlightNo
      AgentID
      Remark
      Pieces
      Weight
      Status
      LockDate
      LockBy
      BillComplete
      BillType
      State
      Express
      CoLoaderName
      AgentName
    }
  }
`
