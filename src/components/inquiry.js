import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import TextFieldDisplay from './common/textFieldDisplay'

import { makeStyles } from '@material-ui/core/styles'
import { GET_CON } from './graphql/consignment'
import { SEARCH_BILLINGHEADER } from './graphql/billing'
import { useMutation, useQuery } from '@apollo/react-hooks'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import Loading from './common/loading'
import PrintIcon from '@material-ui/icons/Print'
import PodDialog from './despatching/podDialog'
import BackupIcon from '@material-ui/icons/Backup'
import Divider from '@material-ui/core/Divider'
import TableHalf from './common/tableHalf'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  displayDiv: {
    background: theme.palette.background.paper,
    minHeight: '340px',
    margin: 20,
    marginTop: 30,
  },
  small: {
    width: 300,
  },
  big: {
    width: 800,
  },
  dialog: {
    boxShadow: '0 8px 6px -6px black',
    position: 'static',
    left: '20%',
    top: '10%',
  },
  table: {
    padding: 0,
  },
}))

const tableHead = [
  {
    id: 'ItemCode',
    numeric: false,
    disablePadding: true,
    label: 'Item Code',
    align: 'left',
  },
  {
    id: 'Title',
    numeric: false,
    disablePadding: true,
    label: 'Title',
    align: 'left',
  },
  {
    id: 'Quantity',
    numeric: true,
    disablePadding: true,
    label: 'Quantity',
    align: 'center',
  },
  {
    id: 'UnitPrice',
    numeric: true,
    currency: true,
    disablePadding: true,
    label: 'Unit Price',
    align: 'right',
  },
  {
    id: 'Amount',
    numeric: true,
    currency: true,
    disablePadding: false,
    label: 'Amount',
    align: 'right',
  },
  {
    id: 'lastUpdatedBy',
    disablePadding: true,
    label: 'Last Updated',
    align: 'left',
  },
]
export default function Inquiry(props) {
  const classes = useStyles()
  const [awb, setAwb] = useState({})
  const [selected, setSelected] = useState([])
  const [confirm, setConfirm] = useState(false)
  const {
    loading,
    data: { searchbillingheader: billingheader } = { billingheader: false },
    refetch,
  } = useQuery(SEARCH_BILLINGHEADER, {
    variables: { id: props.location.pathname.split('/')[2] },
  })

  const { data: { consignment } = { consignment: [] } } = useQuery(GET_CON, {
    variables: { id: props.location.pathname.split('/')[2] },
  })

  const [open, setOpen] = useState(false)
  const [openImage, setOpenImage] = useState(false)
  const [totalAmount, setTotalAmount] = useState(0)

  useEffect(() => {
    if (!billingheader) return
    if (billingheader.BillingDetails.length == 0) return

    const totalAmount = billingheader.BillingDetails.reduce(
      (a, b) => a + b.Amount,
      0
    )
    setTotalAmount(totalAmount)
  }, [billingheader])

  const handleUploadDialog = () => {
    setOpen(true)
  }

  const handleShowDialog = () => {
    setOpenImage(!openImage)
  }

  const handleShipmentAdvice = () => {
    axios
      .get(`${restApi}/billing/shipmentadvice/${billingheader.ConsignmentID}`, {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/pdf',
        },
      })
      .then((response) => {
        /* const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute(
          'download',
          `ShipmentAdvice-${billingheader.ConsignmentNo}.pdf`
        ) //or any other extension
        document.body.appendChild(link)
        link.click() */
        var file = new Blob([response.data], { type: 'application/pdf' })
        var fileURL = URL.createObjectURL(file)
        window.open(fileURL, '_blank')
      })
      .catch((error) => console.log(error))
  }

  if (loading)
    return (
      <div style={{ padding: 20 }}>
        <Loading />
      </div>
    )

  if (!loading && !consignment && !billingheader)
    return (
      <div
        style={{
          padding: 20,
        }}
      >
        Consignment Not Found
      </div>
    )

  const awbFormat = (awb) =>
    (awb && awb.replace(/(\w{3})(\w{4})(\w{4})/, '$1-$2-$3')) || ''

  return (
    <div className={classes.displayDiv}>
      <PodDialog
        key={0}
        setOpen={setOpen}
        open={open}
        data={billingheader}
        refetch={refetch}
      ></PodDialog>
      <Grid container spacing={3}>
        <Grid item md={8} xs={12} style={{ padding: 30, paddingTop: 10 }}>
          <Grid container spacing={3}>
            <Grid item md={12} xs={12}>
              <h2 style={{ marginBottom: 5 }}>Consignment Note</h2>

              <Divider />
            </Grid>
          </Grid>
          {!billingheader && consignment && (
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="CN No"
                  value={consignment.ConsignmentNo}
                />
                <TextFieldDisplay label="Pieces" value={consignment.Pieces} />

                {/*  <TextFieldDisplay
                  label="ETD"
                  value={
                    consignment.ETD &&
                    new Intl.DateTimeFormat('en-GB').format(
                      new Date(consignment.ETD)
                    )
                  }
                /> */}
                <TextFieldDisplay
                  label="Dest"
                  value={consignment.Destination}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="HAWB No"
                  value={awbFormat(consignment.AWB)}
                />
                <TextFieldDisplay label="Weight" value={consignment.Weight} />
                {/*  <TextFieldDisplay
                  label="ETA"
                  value={
                    consignment.ETA &&
                    new Intl.DateTimeFormat('en-GB').format(
                      new Date(consignment.ETA)
                    )
                  }
                /> */}
                <TextFieldDisplay
                  label="Created On"
                  value={
                    consignment.CreateOn &&
                    consignment.CreateBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(consignment.CreateOn))
                  }
                />

                <TextFieldDisplay
                  label="Last Updated By"
                  value={
                    consignment.ModifiedBy &&
                    consignment.LastModified &&
                    consignment.ModifiedBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(consignment.LastModified))
                  }
                />
              </Grid>
            </Grid>
          )}
          {billingheader && (
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="CN No"
                  value={billingheader.ConsignmentNo}
                />
                <TextFieldDisplay label="Pieces" value={billingheader.Pieces} />
                <TextFieldDisplay label="Weight" value={billingheader.Weight} />

                <TextFieldDisplay label="Dest" value={billingheader.POD} />
                <TextFieldDisplay
                  label="Created On"
                  value={
                    consignment.CreateOn &&
                    consignment.CreateBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(consignment.CreateOn))
                  }
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="MAWB No"
                  value={awbFormat(billingheader.MAWBNo)}
                />
                <TextFieldDisplay
                  label="HAWB No"
                  value={awbFormat(billingheader.AWBNo)}
                />

                <TextFieldDisplay
                  label="ETD"
                  value={
                    billingheader.ETD &&
                    new Intl.DateTimeFormat('en-GB').format(
                      new Date(billingheader.ETD)
                    )
                  }
                />
                <TextFieldDisplay
                  label="ETA"
                  value={
                    billingheader.ETA &&
                    new Intl.DateTimeFormat('en-GB').format(
                      new Date(billingheader.ETA)
                    )
                  }
                />

                <TextFieldDisplay
                  label="Last Updated By"
                  value={
                    consignment.ModifiedBy &&
                    consignment.LastModified &&
                    consignment.ModifiedBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(consignment.LastModified))
                  }
                />
              </Grid>
            </Grid>
          )}
          <Grid container spacing={3}>
            <Grid item md={12} xs={12}>
              <h3 style={{ marginBottom: 5 }}>Billing Details</h3>

              <Divider />
            </Grid>
          </Grid>
          {billingheader && (
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  multiline
                  label="Shipper"
                  value={
                    billingheader.ShipperCode &&
                    `${billingheader.ShipperCode} ${billingheader.ShipperName}`
                  }
                />
                <TextFieldDisplay
                  multiline
                  label="Bill To"
                  value={
                    billingheader.BillToCode &&
                    `${billingheader.BillToCode} ${billingheader.BillToName2}`
                  }
                />

                <TextFieldDisplay label="PP/CC" value={billingheader.PPorCC} />

                <TextFieldDisplay
                  label="Invoice No"
                  value={billingheader.InvoiceNo}
                />

                <TextFieldDisplay
                  label="Bill By"
                  value={
                    billingheader.BillDate &&
                    billingheader.BillBy &&
                    billingheader.BillBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(billingheader.BillDate))
                  }
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  multiline
                  label="Consignee"
                  value={
                    billingheader.ConsigneeCode
                      ? `${billingheader.ConsigneeCode} ${billingheader.ConsigneeName}`
                      : billingheader.Remark
                  }
                />

                <TextFieldDisplay
                  multiline
                  label="Invoice To"
                  value={
                    billingheader.InvoiceToCode &&
                    `${billingheader.InvoiceToCode} ${billingheader.InvoiceToName2}`
                  }
                />

                <TextFieldDisplay label="Parcel" value={billingheader.Parcel} />
                <TextFieldDisplay
                  label="Invoice Date"
                  value={
                    billingheader.InvoiceDate &&
                    new Intl.DateTimeFormat('en-GB').format(
                      new Date(billingheader.InvoiceDate)
                    )
                  }
                />
                <TextFieldDisplay
                  label="Last Updated By"
                  value={
                    billingheader.lastUpdatedBy &&
                    billingheader.lastUpdated &&
                    billingheader.lastUpdatedBy +
                      ' ' +
                      new Intl.DateTimeFormat('en-GB', {
                        hour: 'numeric',
                        minute: 'numeric',
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour12: true,
                      }).format(new Date(billingheader.lastUpdated))
                  }
                />
              </Grid>
            </Grid>
          )}
          {billingheader && (
            <Grid container spacing={3}>
              <Grid item md={12} xs={12} style={{ paddingLeft: 12 }}>
                <br />
                <Divider />
                <Table
                  className={classes.table}
                  size="small"
                  aria-label="a dense table"
                >
                  <TableHead>
                    <TableRow>
                      {tableHead.map((header, index) => (
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: header.align,
                          }}
                          key={index}
                        >
                          {header.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {billingheader.BillingDetails.map((row) => (
                      <TableRow key={row.id}>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                          component="th"
                          scope="row"
                        >
                          {row.ItemCode}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                        >
                          {row.Title}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: 'center',
                          }}
                        >
                          {row.Quantity}
                        </TableCell>
                        <TableCell
                          style={{
                            textAlign: 'right',
                            paddingLeft: 0,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                        >
                          {row.UnitPrice && row.UnitPrice.toFixed(2)}
                        </TableCell>
                        <TableCell
                          style={{
                            textAlign: 'right',
                            paddingLeft: 0,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                        >
                          {row.Amount && row.Amount.toFixed(2)}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                            width: 200,
                          }}
                        >
                          {row.lastUpdated && row.lastUpdatedBy}{' '}
                          {row.lastUpdated &&
                            new Intl.DateTimeFormat('en-GB', {
                              hour: 'numeric',
                              minute: 'numeric',
                              year: 'numeric',
                              month: 'numeric',
                              day: 'numeric',
                              hour12: true,
                            }).format(new Date(row.lastUpdated))}
                          {!row.lastUpdated && billingheader.lastUpdatedBy}
                          {!row.lastUpdated &&
                            !billingheader.lastUpdatedBy &&
                            billingheader.BillBy}{' '}
                          {!row.lastUpdated &&
                            row.createdAt &&
                            new Intl.DateTimeFormat('en-GB', {
                              hour: 'numeric',
                              minute: 'numeric',
                              year: 'numeric',
                              month: 'numeric',
                              day: 'numeric',
                              hour12: true,
                            }).format(new Date(row.createdAt))}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
                <div
                  style={{
                    textAlign: 'right',
                    marginTop: 5,
                    marginBottom: 5,
                    paddingRight: 220,
                  }}
                >
                  <b>Total Amount: {parseFloat(totalAmount).toFixed(2)}</b>
                </div>
                <Divider />
                <br /> <br /> <br /> <br /> <br />
              </Grid>
            </Grid>
          )}
        </Grid>

        <Grid
          item
          md={4}
          xs={12}
          style={{ background: '#ededed', minHeight: 350 }}
        >
          {billingheader && (
            <div style={{ paddingTop: 70, paddingLeft: 30 }}>
              <Button
                variant="contained"
                disableElevation
                style={{ marginTop: 20 }}
                color="primary"
                onClick={handleShipmentAdvice}
                startIcon={<PrintIcon />}
              >
                Print Shipment Advice
              </Button>
              <br />
              <Button
                variant="contained"
                disableElevation
                style={{ marginTop: 5 }}
                color="primary"
                onClick={handleUploadDialog}
                startIcon={<BackupIcon />}
              >
                Upload POD
              </Button>
            </div>
          )}
          {/* <TextFieldDisplay
            style={{ marginTop: 20 }}
            label="Image"
            value={billingheader.Image}
          /> */}
          <div style={{ marginTop: 20, paddingLeft: 30 }}>
            {billingheader && billingheader.Image && (
              <img
                className={classes.small}
                src={`${restApi}/podmedia/${billingheader.Image}`}
                onClick={handleShowDialog}
                alt="no image"
              />
            )}
            {openImage && (
              <dialog
                className={classes.dialog}
                style={{ position: 'absolute' }}
                open
                onClick={handleShowDialog}
              >
                <img
                  className={classes.big}
                  src={`${restApi}/podmedia/${billingheader.Image}`}
                  onClick={handleShowDialog}
                  alt="no image"
                />
              </dialog>
            )}
          </div>
          {/*  <pre>{JSON.stringify(billingheader, null, 4)}</pre> */}
        </Grid>
      </Grid>
    </div>
  )
}
