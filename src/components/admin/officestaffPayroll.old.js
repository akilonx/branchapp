import React, { useState } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { useQuery, useMutation } from '@apollo/react-hooks'
import TableHalf from '../common/tableHalf'
import { GET_PAYROLLS } from '../graphql/payroll'
import { GET_OFFICESTAFFS } from '../graphql/officestaff'
import ConfirmationDialog from '../common/confirmationDialog'
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects'
import PrintIcon from '@material-ui/icons/Print'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import AutocompleteField from '../common/autocompleteField'
import Loading from '../common/loading'
import PayrollDialog from './officestaffPayrollDialog'
import PayrollDialogDetail from './officestaffPayrollDetailsDialog'
import Report from './report'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 20,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  displayDiv: {
    background: theme.palette.background.paper,
    padding: '20px',
    minHeight: '340px',
  },
  newButton: {
    marginBottom: 10,
  },
}))

export default function OfficeStaffPayroll() {
  const classes = useStyles()
  const [selected, setSelected] = useState([])
  const [open, setOpen] = useState(false)
  const [openUpload, setOpenUpload] = useState(false)
  const [openHeader, setOpenHeader] = useState(false)
  const [confirm, setConfirm] = useState(false)
  const [payroll, setPayroll] = useState()
  const [openPayroll, setOpenPayroll] = useState(false)
  const [openPayrollDetail, setOpenPayrollDetail] = useState(false)

  const {
    data: { payrollheaders } = { payrollheaders: [] },
    refetch,
  } = useQuery(GET_PAYROLLS)

  const {
    loading: loadingStaff,
    data: { officestaffs } = { officestaffs: [] },
    refetch: refetchOfficestaff,
  } = useQuery(GET_OFFICESTAFFS)

  const [staff, setStaff] = useState('')
  const [staffName, setStaffName] = useState('')
  const [quotation, setQuotation] = useState()

  const handleClickOpen = () => {
    setOpenPayroll(true)
  }

  const tableHead = [
    {
      id: 'id',
      numeric: false,
      disablePadding: true,
      label: 'ID',
    },
    {
      id: 'FromDate',
      date: true,
      label: 'From',
      width: 100,
    },
    {
      id: 'ToDate',
      date: true,
      label: 'To',
    },
  ]

  /* const tableButtons = [
    {
      name: 'Active',
      icon: EmojiObjectsIcon,
      method: handleClickActive,
    },
    {
      name: 'Set Prices & Print Quotation',
      icon: AttachMoneyIcon,
      method: handlePrint,
    },
  ] */

  /* if (error) return <p>API ERROR</p> */

  return (
    <div className={classes.root}>
      {/*  <ConfirmationDialog
        action={handleClickDelete}
        confirm={confirm}
        setConfirm={setConfirm}
        message="Continue remove Quote?"
        okButton="Yes"
        title="Continue remove"
      /> */}
      <PayrollDialog
        key={+new Date() + Math.random()}
        /* data={payroll} */
        setPayroll={setPayroll}
        setSelected={setSelected}
        staff={staff}
        staffName={staffName}
        open={openPayroll}
        setOpen={setOpenPayroll}
      />
      <PayrollDialogDetail
        key={+new Date() + Math.random()}
        data={payroll}
        setPayroll={setPayroll}
        setSelected={setSelected}
        staff={staff}
        staffName={staffName}
        open={openPayrollDetail}
        setOpen={setOpenPayrollDetail}
      />
      <Grid container spacing={0}>
        <Grid item xs={12} sm={6}>
          {loadingStaff && <Loading />}
          {!loadingStaff && (
            <AutocompleteField
              name="buyer"
              label="Staff"
              value={staffName}
              options={officestaffs.map(
                (item) =>
                  `${item.Username} — ${item.FirstName} ${item.LastName}`
              )}
              onChange={(e, value) => {
                const buyerValue = officestaffs.find(
                  (item) =>
                    `${item.Username} — ${item.FirstName} ${item.LastName}` ===
                    value
                )
                if (buyerValue) {
                  refetch({ UserID: buyerValue.UserID })
                  setStaff(buyerValue.UserID)
                  setStaffName(
                    `${buyerValue.Username} — ${buyerValue.FirstName} ${buyerValue.LastName}`
                  )
                }
              }}
            />
          )}
        </Grid>
      </Grid>
      <Grid container spacing={0}>
        {staff && (
          <Button
            variant="contained"
            style={{ marginTop: 30 }}
            classes={{ root: classes.newButton }}
            color="primary"
            onClick={handleClickOpen}
          >
            New
          </Button>
        )}
        <Grid item xs={12} sm={12}>
          {/* <Report key={+new Date() + Math.random()} customer={customer} /> */}

          {/* <QuotationHeaderDialog
            key={+new Date() + Math.random()}
            setOpenHeader={setOpenHeader}
            setSelected={setSelected}
            setQuotation={setQuotation}
            data={quotation}
            customer={customer}
            openHeader={openHeader}
          />

          <QuotationDialog
            key={+new Date() + Math.random()}
            setOpen={setOpen}
            setSelected={setSelected}
            setQuotation={setQuotation}
            data={quotation}
            open={open}
          /> */}
        </Grid>
        <Grid style={{ marginTop: 30 }} item xs={12} sm={12}>
          {/*  <pre>{JSON.stringify(payroll)}</pre> */}
          <TableHalf
            clickOnSelect={true}
            hideSearch={true}
            hideDelete={true}
            hideChange={true}
            selected={selected}
            setSelected={setSelected}
            tableState={setPayroll}
            tableData={payrollheaders}
            setOpen={setOpenPayrollDetail}
            tableHead={tableHead}
          ></TableHalf>

          {/*  {!loading && customer && quotations.length === 0 && (
            <p>No Quotation found.</p>
          )}
          {!loading && customer && quotations.length > 0 && (
            <TableHalf
              tableButtons={tableButtons}
              setConfirm={setConfirm}
              selected={selected}
              setSelected={setSelected}
              tableState={setQuotation}
              tableData={payrollheaders}
              setOpen={setOpenHeader}
              tableHead={tableHead}
            ></TableHalf>
          )} */}
        </Grid>
      </Grid>
    </div>
  )
}
