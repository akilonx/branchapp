import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'
import useForm from 'react-hook-form'
import Slide from '@material-ui/core/Slide'
import { GET_CARTS } from '../graphql/cart'
import { GET_SHOPCONFIG } from '../graphql/config'
import Cookies from 'js-cookie'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { useMutation, useQuery } from '@apollo/react-hooks'
import axios from 'axios'
import PasswordField from 'material-ui-password-field'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    paddingTop: 0,
  },
  dialogPaper: {
    background: theme.palette.primary.paper,
    overflowY: 'visible',
    width: 'auto!important',
  },
  dialogTitle: {
    background: theme.palette.primary.backgroundColor,
    color: theme.palette.primary.main,
  },
  dialogContent: {
    background: theme.palette.primary.backgroundColor,
    overflowY: 'visible',
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.primary.main,
  },
  dialogContentRoot: { padding: 0 },
  dialogActions: {
    padding: theme.spacing(3),
  },
}))

export default function LoginDialog(props) {
  const classes = useStyles()
  const { handleSubmit, register, errors, setValue } = useForm()
  const [password, setPassword] = useState()
  const [inputType, setInputType] = useState('password')
  const onSubmit = (values) => {
    if (!password) return
    /* setInputType('text') */
    props.setOpen(false)
    props.action(password)
  }

  const onFocus = (event) => {
    if (event.target.autocomplete) {
      event.target.autocomplete = 'whatever'
    }
  }

  return (
    <div>
      <Dialog
        fullWidth={true}
        maxWidth="xxs"
        scroll="body"
        open={props.open}
        onClose={() => {
          props.setOpen(false)
        }}
        disableBackdropClick={false}
        classes={{ paper: classes.dialogPaper }}
        aria-labelledby="order-dialog"
      >
        <DialogTitle className={classes.dialogTitle} id="order-dialog">
          {props.dialogTitle}
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              props.setOpen(false)
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>

        <DialogContent classes={{ root: classes.dialogContentRoot }}>
          <div className={classes.root}>
            <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
              {/* <TextField
                type={inputType}
                name="test"
                label={props.title}
                margin="dense"
                autoFocus={true}
                autoComplete="off"
                onFocus={onFocus}
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
              /> */}
              <PasswordField
                hintText="At least 8 characters"
                floatingLabelText={props.title}
                errorText="Your password is too short"
                error={!password}
                onChange={(e) => {
                  if (!e.target.value) return
                  setPassword(e.target.value)
                }}
              />
              <br />
              <br />
              <Button
                type="submit"
                variant="contained"
                disableElevation
                color="primary"
              >
                Submit
              </Button>
            </form>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  )
}
