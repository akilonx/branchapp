INSERT INTO `ao_freightconfig` (`ParmKey`, `ParamDesc`, `ParamValue`) VALUES ('BOOKINGINTERVAL', 'Booking Interval', '15');
INSERT INTO `ao_freightconfig` (`ParmKey`, `ParamDesc`, `ParamValue`) VALUES ('BOOKINGOPENINGHOURS', 'Opening Hours', '10:00:00');
INSERT INTO `ao_freightconfig` (`ParmKey`, `ParamDesc`, `ParamValue`) VALUES ('BOOKINGCLOSINGHOURS', 'Closing Hours', '21:00:00');
INSERT INTO `ao_freightconfig` (`ParmKey`, `ParamDesc`, `ParamValue`) VALUES ('BOOKINGMINHOUR', 'Booking Hour', '60');
ALTER TABLE `spa1`.`ao_bookinglist` ADD COLUMN `BookingNo` VARCHAR(20) NULL AFTER `RatingOn`;
INSERT INTO `spa1`.`ao_freightrunno` (`Prefix`, `LastNo`, `RunDesc`) VALUES ('BOOKING', '20000', 'Booking');
ALTER TABLE `spa1`.`ao_bookinglist`  ADD COLUMN `BookedBy` VARCHAR(20) NULL AFTER `TotalPerson`;